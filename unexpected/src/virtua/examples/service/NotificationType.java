package virtua.examples.service;

public enum NotificationType {
	
	NEW, UPDATE, DELETE;

}
