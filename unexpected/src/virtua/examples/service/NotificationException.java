package virtua.examples.service;

public class NotificationException extends Exception {

	public NotificationException(String string) {
		super(string);
	}

	public NotificationException(String string,
			Exception e) {
		super(string, e);
	}

}
