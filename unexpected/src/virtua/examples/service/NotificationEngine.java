package virtua.examples.service;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import virtua.examples.faces.model.Widget;

@ManagedBean
@ApplicationScoped
public class NotificationEngine {

	public void publish(NotificationType new1, Widget widget) throws NotificationException {
		throw new NotificationException(
				"Unable to send notification; connection unavailable.",
				new NullPointerException());
	}

}
