package virtua.examples.service;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import virtua.examples.faces.model.Widget;

@ManagedBean
@ApplicationScoped
public class NotificationService {
	
	@ManagedProperty("#{notificationEngine}")
	private NotificationEngine notifcationEngine;

	public NotificationEngine getNotifcationEngine() {
		return notifcationEngine;
	}

	public void setNotifcationEngine(NotificationEngine notifcationEngine) {
		this.notifcationEngine = notifcationEngine;
	}

	public void publishNewWidgetEvent(Widget widget)
			throws NotificationException {
		try {
			notifcationEngine.publish(NotificationType.NEW, widget);
		} catch (NotificationException e) {
			e.printStackTrace();
			throw e;
		}
	}

}
