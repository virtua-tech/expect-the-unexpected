package virtua.examples.faces.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Widget implements Serializable {

	private static final long serialVersionUID = 1613955265044592029L;

	private String name;
	private Date createdDate;
	private Integer size;
	private boolean inspected;
	private boolean approved;
	private String color;
	private Integer unitsSold;
	private List<Widget> relatedWidgets;
	
	public Widget() {}

	public Widget(String name, Integer size, boolean approved) {
		setName(name);
		setSize(size);
		setApproved(approved);
		setCreatedDate(new Date());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public boolean isInspected() {
		return inspected;
	}

	public void setInspected(boolean inspected) {
		this.inspected = inspected;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getUnitsSold() {
		return unitsSold;
	}

	public void setUnitsSold(Integer unitsSold) {
		this.unitsSold = unitsSold;
	}

	public void setRelatedWidgets(List<Widget> relatedWidgets) {
		this.relatedWidgets = relatedWidgets;
	}

	public List<Widget> getRelatedWidgets() {
		return relatedWidgets;
	}
	
}
