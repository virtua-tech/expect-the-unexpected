/*
 * Copyright 2009 Prime Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package virtua.examples.faces.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.DualListModel;

@ManagedBean
@ViewScoped
public class PickListBean implements Serializable {

	private DualListModel<String> cities;

	public PickListBean() {
		
		//Cities
		List<String> citiesSource = new ArrayList<String>();
		List<String> citiesTarget = new ArrayList<String>();
		
		citiesSource.add("Istanbul");
		citiesSource.add("Ankara");
		citiesSource.add("Izmir");
		citiesSource.add("Antalya");
		citiesSource.add("Bursa");
		
		cities = new DualListModel<String>(citiesSource, citiesTarget);
	}
	
	
	public DualListModel<String> getCities() {
		return cities;
	}
	public void setCities(DualListModel<String> cities) {
		this.cities = cities;
	}
}