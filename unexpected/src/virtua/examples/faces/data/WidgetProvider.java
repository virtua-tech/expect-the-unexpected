package virtua.examples.faces.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import virtua.examples.faces.model.Widget;
import virtua.examples.service.NotificationService;

@ManagedBean(eager = true)
@ApplicationScoped
public class WidgetProvider implements Serializable {

	private static final long serialVersionUID = -4333931883643833214L;
	private List<Widget> widgets = new ArrayList<Widget>();
	@ManagedProperty("#{notificationService}")
	private NotificationService notifcationService;

	public NotificationService getNotifcationService() {
		return notifcationService;
	}

	public void setNotifcationService(NotificationService notifcationService) {
		this.notifcationService = notifcationService;
	}

	public List<Widget> getWidgets(int size) {
		if (widgets == null || widgets.size() != size) {
			widgets = new ArrayList<Widget>();
			for (int i = 0; i < size; i++) {
				Widget widget = new Widget();
				widget.setCreatedDate(new Date());
				widget.setName("Widget " + i);
				widget.setColor("red");
				widget.setSize(100 + i);
				widgets.add(widget);
				List<Widget> relatedWidgets = new ArrayList<Widget>();
				for (int x = 0; i < 2; i++) {
					Widget relatedWidget = new Widget();
					relatedWidget.setCreatedDate(new Date());
					relatedWidget.setName("Related Widget " + x);
					relatedWidget.setColor("red");
					relatedWidget.setSize(100 + x);
					relatedWidgets.add(relatedWidget);
				}
				widget.setRelatedWidgets(relatedWidgets);
			}

		}
		return widgets;
	}

	public void add(Widget widget) throws DatabaseException {
		try {
			widgets.add(widget);
			notifcationService.publishNewWidgetEvent(widget);	
		} catch (Exception e) {
			e.printStackTrace();
			throw new DatabaseException("Error connecting to database", e);
		}
	}

	public void remove(Widget widget) {
		widgets.remove(widget);
	}
}
