package virtua.examples.faces.data.primefaces;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import virtua.examples.faces.model.Widget;

public class WidgetDataModel extends ListDataModel<Widget>
		implements SelectableDataModel<Widget> {

	private static final long serialVersionUID = 505926812439624360L;

	public WidgetDataModel() {
	}

	public WidgetDataModel(List<Widget> data) {
		super(data);
	}

	@Override
	public Widget getRowData(String rowKey) {
		// In a real app, a more efficient way like a query by rowKey should be
		// implemented to deal with huge data

		@SuppressWarnings("unchecked")
		List<Widget> widgets = (List<Widget>) getWrappedData();

		for (Widget widget : widgets) {
			if (getRowKey(widget).equals(rowKey))
				return widget;
		}

		return null;
	}

	@Override
	public String getRowKey(Widget widget) {
		return widget.getName() + widget.getCreatedDate();
	}
}