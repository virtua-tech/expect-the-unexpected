package virtua.examples.faces.data;

public class DatabaseException extends Exception {

	private static final long serialVersionUID = -2381901216165659598L;

	public DatabaseException(String string) {
		super(string);
	}

	public DatabaseException(String string, Exception e) {
		super(string, e);
	}

}
