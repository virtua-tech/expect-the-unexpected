package virtua.examples.faces.el;

import java.beans.FeatureDescriptor;
import java.util.Iterator;

import javax.el.ELContext;
import javax.el.ELException;
import javax.el.ELResolver;
import javax.el.PropertyNotFoundException;
import javax.el.PropertyNotWritableException;

public class CustomELResolver extends ELResolver {

	@Override
	public Class<?> getCommonPropertyType(ELContext context, Object property) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context,
			Object property) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Class<?> getType(ELContext context, Object base, Object property)
			throws NullPointerException, PropertyNotFoundException, ELException {
        if (base != null)
            return null;
        if (property == null)
            throw new PropertyNotFoundException();
        if (!(property instanceof String))
            return null;
		return null;
	}

	@Override
	public Object getValue(ELContext context, Object base, Object property)
			throws NullPointerException, PropertyNotFoundException, ELException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isReadOnly(ELContext context, Object base, Object property)
			throws NullPointerException, PropertyNotFoundException, ELException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setValue(ELContext context, Object base, Object property, Object value)
			throws NullPointerException, PropertyNotFoundException,
			PropertyNotWritableException, ELException {
		// TODO Auto-generated method stub

	}
	
	   protected String castAndIntern(Object o)
	    {
	        String s = (String)o;
	        return s.intern();
	    }

}
