package virtua.examples.faces.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.primefaces.event.SelectEvent;

import virtua.examples.faces.data.DatabaseException;
import virtua.examples.faces.data.WidgetProvider;
import virtua.examples.faces.data.primefaces.WidgetDataModel;
import virtua.examples.faces.model.Widget;

@ManagedBean(name = "widgetViewer")
@ViewScoped
public class WidgetViewerBean implements Serializable {

	private static final long serialVersionUID = -311190670489073276L;

	private static final Logger logger = Logger
			.getLogger(WidgetViewerBean.class.getName());

	@ManagedProperty(value = "#{widgetProvider}")
	private WidgetProvider widgetProvider;

	private Widget selectedWidget;
	private String selectedWidgetName;
	private int listSize = 20;

	private int page;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	@PostConstruct
	public void init() {

	}

	public int getListSize() {
		return listSize;
	}

	public void setListSize(int listSize) {
		this.listSize = listSize;
	}

	public WidgetProvider getWidgetProvider() {
		return widgetProvider;
	}

	public void setWidgetProvider(WidgetProvider widgetProvider) {
		this.widgetProvider = widgetProvider;
	}

	public WidgetDataModel getCurrentWidgets() {
		return new WidgetDataModel(widgetProvider.getWidgets(listSize));
	}

	public WidgetDataModel getEmptyWidgetList() {
		return new WidgetDataModel(widgetProvider.getWidgets(0));
	}

	public void sizeChanged(ValueChangeEvent e) {
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage("Value changed from " + e.getOldValue()
						+ " to " + e.getNewValue()));
	}

	@SuppressWarnings("unchecked")
	public void selectRandomWidget() {
		int widgetIndex = (int) (Math.random() * getListSize());
		setSelectedWidget(((List<Widget>) getCurrentWidgets().getWrappedData())
				.get(widgetIndex));
		setSelectedWidgetName(getSelectedWidget().getName());
		display("Selected random widget #" + widgetIndex);
	}

	public void setSelectedWidget(Widget selectedWidget) {
		this.selectedWidget = selectedWidget;
	}

	public Widget getSelectedWidget() {
		if (selectedWidget == null) {
			selectedWidget = new Widget();
		}
		return selectedWidget;
	}

	@SuppressWarnings("unchecked")
	public void selectWidget() {
		setSelectedWidget(((List<Widget>) getCurrentWidgets().getWrappedData())
				.get(5));
		display("Selected widget #5");
	}

	@SuppressWarnings("unchecked")
	public void selectWidget(long index, boolean updatePage) {
		setSelectedWidget(((List<Widget>) getCurrentWidgets().getWrappedData())
				.get((int) index));
		display("Selected widget #" + index);
		if (updatePage) {
			page++;
			display("New page #:" + page);
		}
	}

	public List<Widget> autocomplete(String query) {
		List<Widget> suggestions = new ArrayList<Widget>();

		for (Widget widget : widgetProvider.getWidgets(listSize)) {
			if (widget.getName().toLowerCase().startsWith(query))
				suggestions.add(widget);
		}

		return suggestions;
	}

	public List<String> autocompleteNames(String query) {
		List<String> suggestions = new ArrayList<String>();

		for (Widget widget : widgetProvider.getWidgets(listSize)) {
			if (widget.getName().toLowerCase().startsWith(query))
				suggestions.add(widget.getName());
		}

		return suggestions;
	}

	public void onSelect(SelectEvent e) {
		display("onSelect event handler called");
	}

	public void next() {
		int row = widgetProvider.getWidgets(listSize).indexOf(
				getSelectedWidget());
		selectWidget(row + 1, true);
	}

	public void previous() {
		int row = widgetProvider.getWidgets(listSize).indexOf(
				getSelectedWidget());
		selectWidget(row - 1, true);
	}

	public void setSelectedWidgetName(String selectedWidgetName) {
		this.selectedWidgetName = selectedWidgetName;
	}

	public String getSelectedWidgetName() {
		return selectedWidgetName;
	}

	protected void display(String message) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(message));
	}

	protected void displayError(String message) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
	}

	public void save() {
		try {
			getWidgetProvider().add(selectedWidget);
			display("Your widget has been created successfully");
			setSelectedWidget(null);
		} catch (DatabaseException e) {
			displayError("Sorry, a database error has occurred. Please try again later.");
			logger.log(Level.SEVERE, "Error accessing the database", e);
		}
	}

	public void unrecoverableSave() throws DatabaseException {
		getWidgetProvider().add(selectedWidget);
		display("Your widget has been created successfully");
		setSelectedWidget(null);
	}
}