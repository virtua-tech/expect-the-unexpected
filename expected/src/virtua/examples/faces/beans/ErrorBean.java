package virtua.examples.faces.beans;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class ErrorBean implements Serializable {

    private static final long serialVersionUID = -4000064752200126572L;
    
    private static final Logger logger = Logger.getLogger(ErrorBean.class
            .getName());

    public void logClientException(String message, String url, String line) {
        if (message != null) {
            logger.log(Level.SEVERE, "JavaScript error: " + message
                    + "; URL = " + url + " (line " + line + ")");
        }
    }
}