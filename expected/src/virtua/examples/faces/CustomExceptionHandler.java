package virtua.examples.faces;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.FacesException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

import virtua.examples.faces.beans.WidgetViewerBean;

public class CustomExceptionHandler extends ExceptionHandlerWrapper {

	private static final Logger logger = Logger
			.getLogger(WidgetViewerBean.class.getName());
	private ExceptionHandler wrapped;

	public CustomExceptionHandler(ExceptionHandler wrapped) {
		this.wrapped = wrapped;
	}

	@Override
	public void handle() throws FacesException {
		Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents()
				.iterator();
		while (i.hasNext()) {
			ExceptionQueuedEvent event = i.next();
			ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event
					.getSource();
			Throwable t = context.getException();
			try {
				logger.log(Level.SEVERE, "Serious error happened!", t);
			} finally {
				i.remove();
			}
		}
		getWrapped().handle();
	}

	@Override
	public ExceptionHandler getWrapped() {
		return this.wrapped;
	}
}
